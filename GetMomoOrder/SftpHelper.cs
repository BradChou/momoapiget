﻿using System;
using System.IO;
using System.Collections;
using Renci.SshNet;

namespace GetMomoOrder
{
    /// <summary>
    /// SFTP操作類
    /// </summary>
    public class SFTPHelper
    {
        #region 欄位或屬性
        private SftpClient sftp;
        /// <summary>
        /// SFTP連線狀態
        /// </summary>
        public bool Connected { get { return sftp.IsConnected; } }
        #endregion

        #region 構造
        /// <summary>
        /// 構造
        /// </summary>
        /// <param name="host">連線路徑</param>
        /// <param name="port">埠</param>
        /// <param name="user">使用者名稱</param>
        /// <param name="pwd">密碼</param>
        public SFTPHelper(string host, string port, string user, string pwd)
        {
            sftp = new SftpClient(host, Int32.Parse(port), user, pwd);
        }
        #endregion

        #region 連線SFTP
        /// <summary>
        /// 連線SFTP
        /// </summary>
        /// <returns>true成功</returns>
        public bool Connect()
        {
            try
            {
                if (!Connected)
                {
                    sftp.Connect();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("連線SFTP失敗，原因：{0}", ex.Message));
            }
        }
        #endregion

        #region 斷開SFTP
        /// <summary>
        /// 斷開SFTP
        /// </summary> 
        public void Disconnect()
        {
            try
            {
                if (sftp != null && Connected)
                {
                    sftp.Disconnect();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("斷開SFTP失敗，原因：{0}", ex.Message));
            }
        }
        #endregion

        #region SFTP上傳檔案
        /// <summary>
        /// SFTP上傳檔案
        /// </summary>
        /// <param name="localPath">本地路徑</param>
        /// <param name="remotePath">遠端路徑</param>
        public void Upload(string localPath, string remotePath)
        {
            try
            {
                using (var file = File.OpenRead(localPath))
                {
                    Connect();
                    sftp.UploadFile(file, remotePath, null);
                    Disconnect();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SFTP檔案上傳失敗，原因：{0}", ex.Message));
            }
        }
        #endregion

        #region SFTP獲取檔案
        /// <summary>
        /// SFTP獲取檔案
        /// </summary>
        /// <param name="remotePath">遠端路徑</param>
        /// <param name="localPath">本地路徑</param>
        public void Download(string remotePath, string localPath)
        {
            try
            {
                Connect();
                var byt = sftp.ReadAllBytes(remotePath);
                Disconnect();
                File.WriteAllBytes(localPath, byt);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SFTP檔案獲取失敗，原因：{0}", ex.Message));
            }

        }
        #endregion

        #region 刪除SFTP檔案
        /// <summary>
        /// 刪除SFTP檔案 
        /// </summary>
        /// <param name="remoteFile">遠端路徑</param>
        public void Delete(string remoteFile)
        {
            try
            {
                Connect();
                sftp.Delete(remoteFile);
                Disconnect();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SFTP檔案刪除失敗，原因：{0}", ex.Message));
            }
        }
        #endregion

        #region 獲取SFTP檔案列表
        /// <summary>
        /// 獲取SFTP檔案列表
        /// </summary>
        /// <param name="remotePath">遠端目錄</param>
        /// <param name="fileSuffix">檔案字尾</param>
        /// <returns></returns>
        public ArrayList GetFileList(string remotePath, string fileSuffix)
        {
            try
            {
                Connect();
                var files = sftp.ListDirectory(remotePath);
                Disconnect();
                var objList = new ArrayList();
                foreach (var file in files)
                {
                    string name = file.Name;
                    if (name.Length > (fileSuffix.Length + 1) && fileSuffix == name.Substring(name.Length - fileSuffix.Length))
                    {
                        objList.Add(name);
                    }
                }
                return objList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SFTP檔案列表獲取失敗，原因：{0}", ex.Message));
            }
        }
        #endregion

        #region 移動SFTP檔案
        /// <summary>
        /// 移動SFTP檔案
        /// </summary>
        /// <param name="oldRemotePath">舊遠端路徑</param>
        /// <param name="newRemotePath">新遠端路徑</param>
        public void Move(string oldRemotePath, string newRemotePath)
        {
            try
            {
                Connect();
                sftp.RenameFile(oldRemotePath, newRemotePath);
                Disconnect();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SFTP檔案移動失敗，原因：{0}", ex.Message));
            }
        }
        #endregion

        #region 移動SFTP當前目錄
        /// <summary>
        /// 移動SFTP當前目錄
        /// </summary>
        /// <param name="RemotePath">遠端路徑</param>

        public void ChangeDirectory(string RemotePath)
        {
            try
            {
                Connect();
                sftp.ChangeDirectory(RemotePath);
                Disconnect();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("移動當前目錄失敗，原因：{0}", ex.Message));
            }
        }
        #endregion
    }
}
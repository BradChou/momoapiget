﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetMomoOrder
{
    public class momolog 
    {
        string connectionString = ConfigurationManager.ConnectionStrings["JunfuSqlServer"].ToString();


        public void insertmomolog(string type, string action, string message) 
        {
            using (var cn = new SqlConnection(connectionString))
            {
                momologModel _m = new momologModel()
                {
                    type = type,
                    action = action,
                    message = message,
                    cdate = DateTime.Now
                };


                string sql = @"INSERT INTO [MOMOLog](type,action,message,cdate) VALUES (@type,@action,@message,@cdate)";
                cn.Execute(sql, _m);
                
            }

        }

        public void insert(string sql)
        {
            using (var cn = new SqlConnection(connectionString))
            {
                try
                {
                    cn.Execute(sql, commandTimeout: 100);
                }
                catch (Exception)
                {

                    throw;
                }
              

            }

        }




    }


    public class momologModel
    {
        public int? id { get; set; }
        public string type { get; set; }
        public string action { get; set; }
        public string message { get; set; }
        public DateTime? cdate { get; set; }
    }
}

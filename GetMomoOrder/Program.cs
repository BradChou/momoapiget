﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace GetMomoOrder
{
    class Program
    {
        private static void Main(string[] args)
        {
            momolog _momolog = new momolog();
            Notification_DAL _Notification_DAL = new Notification_DAL();
            CBMDetailLog_DA _CBMDetailLog_DA = new CBMDetailLog_DA();
            string momoget = "1000";

            _momolog.insertmomolog(momoget, "Start", "OK");

            try
            {
                SFTPHelper sftpHelper = new SFTPHelper(ConfigurationManager.AppSettings["SFTPPath"], ConfigurationManager.AppSettings["SFTPPort"], ConfigurationManager.AppSettings["SFTPUsername"], ConfigurationManager.AppSettings["SFTPPassword"]);
                string remotePath = "/momo_to_fse/";
                string remoteBackUpPath = "/momo_to_fse/backup/";
                string downloadPath = ConfigurationManager.AppSettings["DownloadPath"];
                ArrayList MOMOToFSEFiles = sftpHelper.GetFileList(remotePath, "txt");
                foreach (var file in MOMOToFSEFiles)
                {
                    _momolog.insertmomolog(momoget, "開始抓FTP檔案", file.ToString());
                    sftpHelper.Download(remotePath + file, downloadPath + file);
                    _momolog.insertmomolog(momoget, "結束抓FTP檔案", file.ToString());
                }

                Encoding encoding = Encoding.GetEncoding("big5");
                string[] files = Directory.GetFiles(ConfigurationManager.AppSettings["DownloadPath"], "*.txt");
                foreach (string path in files)
                {
                    _momolog.insertmomolog(momoget, "開始解析TXT", path);

                    string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(path);
                    string DeliveryType = fileNameWithoutExtension.Substring(8, 1);
                    string fileName = Path.GetFileName(path);
                    _ = string.Empty;
                    string supplier_code = "F35";
                    string supplier_name = "";
                    string customer_code = "F3500010002";
                    string text5 = "";
                    string text6 = "";
                    string text7 = "";
                    string text8 = "";
                    string text9 = "";
                    string text10 = "";
                    SortedList sortedList = new SortedList();
                    using (SqlCommand sqlCommand = new SqlCommand())
                    {
                        sqlCommand.CommandText = "select tbCustomers.* , tbSuppliers.supplier_name  from tbCustomers  left join  tbSuppliers  on tbCustomers.supplier_code = tbSuppliers.supplier_code  where customer_code = '" + customer_code + "'";
                        DataTable dataTable = dbAdapter.getDataTable(sqlCommand);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            supplier_code = dataTable.Rows[0]["supplier_code"].ToString();
                            supplier_name = dataTable.Rows[0]["supplier_name"].ToString();
                            text5 = dataTable.Rows[0]["shipments_city"].ToString();
                            text6 = dataTable.Rows[0]["shipments_area"].ToString();
                            text7 = dataTable.Rows[0]["shipments_road"].ToString();
                        }
                    }
                    using (SqlCommand sqlCommand2 = new SqlCommand())
                    {
                        sqlCommand2.CommandText = "select * from tbPostCityArea with(Nolock)";
                        DataTable dataTable2 = dbAdapter.getDataTable(sqlCommand2);
                        if (dataTable2 != null && dataTable2.Rows.Count > 0)
                        {
                            for (int j = 0; j <= dataTable2.Rows.Count - 1; j++)
                            {
                                text10 = dataTable2.Rows[j]["zip"].ToString();
                                text8 = dataTable2.Rows[j]["city"].ToString();
                                text9 = dataTable2.Rows[j]["area"].ToString();
                                if (sortedList.IndexOfKey(text10) == -1)
                                {
                                    tbPost tbPost = new tbPost();
                                    tbPost.zip = text10;
                                    tbPost.city = text8;
                                    tbPost.area = text9;
                                    sortedList.Add(text10, tbPost);
                                }
                            }
                        }
                    }
                    new List<StringBuilder>();
                    StringBuilder stringBuilder = new StringBuilder();
                    int num = 0;
                    string area_arrive_code = "";
                    string receive_customer_code = "";
                    string receive_contact = "";
                    string receive_address = "";
                    string receive_tel1 = "";
                    int num2 = 0;
                    string invoice_desc = "";
                    string order_number = "";
                    int remote_fee = 0;
                    int num4 = 0;
                    string pricing_type = "02";
                    string check_number = "";
                    string check_type = "001";
                    string subpoena_category = "11";
                    string text22 = "";
                    string receive_city = "";
                    string receive_area = "";
                    string receive_by_arrive_site_flag = "0";
                    string product_category = "001";
                    string time_period = "午";
                    string receipt_flag = "0";
                    string pallet_recycling_flag = "0";
                    string print_date = "";
                    string supplier_date = "";
                    string CbmSize = "";
                    int num5 = 0;
                    string Distributor = "";
                    string send_contact = "";
                    string send_tel = "";
                    string text36 = "";
                    string send_city = "";
                    string send_area = "";
                    string send_address = "";
                    int num6 = -1;
                    string ProductId = "CS000035";//固定

                    string SpecCodeId = "S060";//固定

                    //stringBuilder = new StringBuilder();
                    //stringBuilder.Append("DECLARE @request_id numeric(18, 0);");
                    //stringBuilder.Append("DECLARE @supplier_fee int;");
                    //stringBuilder.Append("DECLARE @cscetion_fee int;");
                    IEnumerable<string> enumerable = File.ReadLines(path, Encoding.Default);
                    int result;
                    List<string> vs = new List<string>();
                    List<string> check_numbers = new List<string>();

                    if (!(DeliveryType == "D"))
                    {
                        if (DeliveryType == "R")
                        {
                            foreach (string item in enumerable)
                            {
                                string text40 = item;
                                if (DeliveryType == "R")
                                {
                                    byte[] bytes = StringToByteArray(item);
                                    text40 = encoding.GetString(bytes);
                                }
                                if (!(text40 == ""))
                                {
                                    area_arrive_code = "";
                                    receive_customer_code = "";
                                    receive_contact = "";
                                    receive_address = "";
                                    receive_tel1 = "";
                                    num2 = 0;
                                    invoice_desc = "";
                                    order_number = "";
                                    check_number = "";
                                    text10 = "";
                                    text22 = "";
                                    receive_city = "";
                                    receive_area = "";
                                    remote_fee = 0;
                                    num4 = 0;
                                    CbmSize = "";
                                    num5 = 0;
                                    Distributor = "";
                                    byte[] bytes2 = Encoding.Default.GetBytes(text40);
                                    Encoding.Default.GetString(bytes2, 0, 4);
                                    if (int.TryParse(Encoding.Default.GetString(bytes2, 4, 8), out result))
                                    {
                                        print_date = MyIntDateToStr(result);
                                        _ = Convert.ToDateTime(print_date).DayOfWeek;
                                        supplier_date = Convert.ToDateTime(print_date).AddDays(1.0).ToString("yyyy/MM/dd");
                                    }
                                    Encoding.Default.GetString(bytes2, 12, 8);
                                    check_number = Encoding.Default.GetString(bytes2, 20, 12);
                                    check_numbers.Add(check_number);
                                    Encoding.Default.GetString(bytes2, 35, 2);
                                    CbmSize = "1";
                                    receive_customer_code = Encoding.Default.GetString(bytes2, 37, 10);
                                    order_number = Encoding.Default.GetString(bytes2, 47, 32);
                                    order_number = order_number.TrimEnd();
                                    send_contact = Encoding.Default.GetString(bytes2, 79, 32);
                                    send_contact = send_contact.TrimEnd();
                                    send_tel = Encoding.Default.GetString(bytes2, 111, 32);
                                    text36 = Encoding.Default.GetString(bytes2, 143, 3);
                                    send_address = Encoding.Default.GetString(bytes2, 146, 64);
                                    if (text36.Length >= 3)
                                    {
                                        num6 = sortedList.IndexOfKey(text36.Substring(0, 3));
                                        if (num6 > -1)
                                        {
                                            tbPost obj = (tbPost)sortedList.GetByIndex(num6);
                                            send_city = obj.city;
                                            send_area = obj.area;
                                            send_address = send_address.Replace("台", "臺");
                                            send_address = send_address.Replace(send_city, "").Replace(send_area, "");
                                        }
                                    }
                                    receive_tel1 = Encoding.Default.GetString(bytes2, 210, 32);
                                    receive_tel1 = receive_tel1.TrimEnd();
                                    receive_contact = Encoding.Default.GetString(bytes2, 242, 32);
                                    string @string = Encoding.Default.GetString(bytes2, 274, 3);
                                    receive_address = Encoding.Default.GetString(bytes2, 277, 64);
                                    if (@string.TrimEnd() != "" && @string.TrimEnd().Length >= 3)
                                    {
                                        text10 = @string;
                                    }
                                    if (text10 != "")
                                    {
                                        num6 = sortedList.IndexOfKey(text10.Substring(0, 3));
                                        if (num6 > -1)
                                        {
                                            tbPost obj2 = (tbPost)sortedList.GetByIndex(num6);
                                            receive_city = obj2.city;
                                            receive_area = obj2.area;
                                            receive_address = receive_address.Replace("台", "臺");
                                            receive_address = receive_address.Replace(receive_city, "").Replace(receive_area, "");
                                        }
                                    }
                                    using (SqlCommand sqlCommand3 = new SqlCommand())
                                    {
                                        sqlCommand3.Parameters.AddWithValue("@receive_city", receive_city);
                                        sqlCommand3.Parameters.AddWithValue("@receive_area", receive_area);
                                        sqlCommand3.CommandText = "usp_GetDistributor";
                                        sqlCommand3.CommandType = CommandType.StoredProcedure;
                                        using (DataTable dataTable3 = dbAdapter.getDataTable(sqlCommand3))
                                        {
                                            if (dataTable3 != null && dataTable3.Rows.Count > 0)
                                            {
                                                try
                                                {
                                                    if (Convert.ToInt32(dataTable3.Rows[0]["status_code"]) == 1)
                                                    {
                                                        Distributor = dataTable3.Rows[0]["supplier_code"].ToString();
                                                        area_arrive_code = dataTable3.Rows[0]["area_arrive_code"].ToString();
                                                    }
                                                }
                                                catch
                                                {
                                                }
                                            }
                                        }
                                    }
                                    string string2 = Encoding.Default.GetString(bytes2, 341, 30);
                                    if (string2.TrimEnd() != "")
                                    {
                                        invoice_desc += string2.TrimEnd();
                                    }
                                    Encoding.Default.GetString(bytes2, 371, 10);
                                    if (int.TryParse(Encoding.Default.GetString(bytes2, 381, 8), out result))
                                    {
                                        MyIntDateToStr(result);
                                    }
                                    switch (Encoding.Default.GetString(bytes2, 389, 2))
                                    {
                                        case "1":
                                            time_period = "早";
                                            break;
                                        case "2":
                                            time_period = "午";
                                            break;
                                        case "3":
                                            time_period = "晚";
                                            break;
                                        case "4":
                                            time_period = "不指定";
                                            break;
                                    }
                                    Encoding.Default.GetString(bytes2, 391, 1);
                                    int.TryParse(Encoding.Default.GetString(bytes2, 392, 8), out num5);
                                    Encoding.Default.GetString(bytes2, 400, 2);
                                    Encoding.Default.GetString(bytes2, 402, 5);
                                    Encoding.Default.GetString(bytes2, 407, 1);
                                    Encoding.Default.GetString(bytes2, 408, 1);
                                    string string3 = Encoding.Default.GetString(bytes2, 409, 60);
                                    if (string3.TrimEnd() != "")
                                    {
                                        invoice_desc += string3.TrimEnd();
                                    }
                                    Encoding.Default.GetString(bytes2, 469, 1);
                                    string string4 = Encoding.Default.GetString(bytes2, 470, 24);
                                    if (string4.TrimEnd() != "")
                                    {
                                        invoice_desc += string4.TrimEnd();
                                    }
                                    Encoding.Default.GetString(bytes2, 494, 1);
                                    Encoding.Default.GetString(bytes2, 495, 1);
                                    Encoding.Default.GetString(bytes2, 496, 1);
                                    Encoding.Default.GetString(bytes2, 497, 2);
                                    Encoding.Default.GetString(bytes2, 499, 13);
                                    if (bytes2.Length > 512)
                                    {
                                        Encoding.Default.GetString(bytes2, 512, 382);
                                    }
                                    num++;
                                    string text41 = @"
DeliveryType,
pricing_type,
customer_code,
check_number,
check_type,
subpoena_category,
receive_tel1,
receive_customer_code,
receive_contact,
receive_city,
receive_area,
receive_address,
area_arrive_code,
receive_by_arrive_site_flag ,
pieces,
plates,
cbm,
collection_money,
arrive_to_pay_freight,
arrive_to_pay_append,
send_contact,
send_tel,
send_city,
send_area,
send_address,
product_category,
invoice_desc,
time_period,
arrive_assign_date,
receipt_flag,
pallet_recycling_flag,
cuser,
cdate,
uuser,
udate,
supplier_code,
supplier_name,
sub_check_number,
print_date,
supplier_date,
order_number,
remote_fee,
print_flag,
turn_board,
upstairs,
difficult_delivery,
turn_board_fee,
upstairs_fee,
difficult_fee, 
Distributor,
CbmSize, 
Less_than_truckload,
import_randomCode,
add_transfer,
ProductId,
SpecCodeId
";
                                    string text42 = 
                                        "N'" + DeliveryType +
                                        "', N'" + pricing_type + 
                                        "', N'" + customer_code + 
                                        "', N'" + check_number + 
                                        "', N'" + check_type + 
                                        "', N'" + subpoena_category + 
                                        "', N'" + receive_tel1 +
                                        "', N'" + receive_customer_code + 
                                        "', N'" + receive_contact +
                                        "', N'" + receive_city +
                                        "', N'" + receive_area +
                                        "', N'" + receive_address +
                                        "', N'" + area_arrive_code + 
                                        "', N'" + receive_by_arrive_site_flag + 
                                        "', N'1'" +
                                        ", N'0'" +
                                        ", N'0'" +
                                        ", N'0'" +
                                        ", N'0'" +
                                        ", N'0'" +
                                        ", N'" + send_contact + 
                                        "', N'" + send_tel + 
                                        "', N'" + send_city + 
                                        "', N'" + send_area + 
                                        "', N'" + send_address +
                                        "', N'" + product_category + 
                                        "', N'" + invoice_desc + 
                                        "', N'" + time_period + 
                                        "', NULL" +
                                        ", N'" + receipt_flag + 
                                        "', N'" + pallet_recycling_flag +
                                        "', N'skyeyes'" +
                                        ", GETDATE()" +
                                        ", N'skyeyes'" +
                                        ", GETDATE()" +
                                        ", N'" + supplier_code +
                                        "', N'" + supplier_name +
                                        "', N'000'" +
                                        ", N'" + print_date + 
                                        "',N'" + supplier_date +
                                        "',N'" + order_number + 
                                        "',N'" + remote_fee + 
                                        "',N'0'" +
                                        ", N'0'" +
                                        ", N'0'" +
                                        ", N'0'" +
                                        ", N'0'" +
                                        ", N'0'" +
                                        ", N'0'" +
                                        ", N'" + Distributor +
                                        "', N'" + CbmSize.ToString() +
                                        "', N'1'" +
                                        ", N'" + fileName +
                                        "', N'0'"+
                                         ", N'" + ProductId +
                                          "', N'" + SpecCodeId + "'"

                                        ;
                                    //stringBuilder.Append();

                                    vs.Add("SET @request_id= 0 ;SET @supplier_fee=0; SET @cscetion_fee=0; INSERT INTO tcDeliveryRequests (" + text41 + ") VALUES(" + text42 + "); Select @request_id=@@Identity ; select @supplier_fee=_fee.supplier_fee   from dbo.fu_GetLTShipFeeByRequestId(@request_id) _fee; Update tcDeliveryRequests set supplier_fee = " + num4 + ",total_fee= " + num4 + " where request_id =@request_id;");
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (string item2 in enumerable)
                        {
                            string text43 = item2;
                            if (DeliveryType == "R")
                            {
                                byte[] bytes3 = StringToByteArray(item2);
                                text43 = encoding.GetString(bytes3);
                            }
                            if (!(text43 == ""))
                            {
                                area_arrive_code = "";
                                receive_customer_code = "";
                                receive_contact = "";
                                receive_address = "";
                                receive_tel1 = "";
                                num2 = 0;
                                invoice_desc = "";
                                order_number = "";
                                check_number = "";
                                text10 = "";
                                text22 = "";
                                receive_city = "";
                                receive_area = "";
                                remote_fee = 0;
                                num4 = 0;
                                CbmSize = "";
                                num5 = 0;
                                Distributor = "";
                                send_contact = "";
                                send_tel = "";
                                text36 = "";
                                send_city = "";
                                send_area = "";
                                send_address = "";
                                subpoena_category = "11";
                                byte[] bytes4 = Encoding.Default.GetBytes(text43);
                                Encoding.Default.GetString(bytes4, 0, 4);
                                if (int.TryParse(Encoding.Default.GetString(bytes4, 4, 8), out result))
                                {
                                    print_date = MyIntDateToStr(result);
                                    _ = Convert.ToDateTime(print_date).DayOfWeek;
                                    supplier_date = Convert.ToDateTime(print_date).AddDays(1.0).ToString("yyyy/MM/dd");
                                }
                                Encoding.Default.GetString(bytes4, 12, 8);
                                check_number = Encoding.Default.GetString(bytes4, 20, 12);
                                check_numbers.Add(check_number);
                                int.TryParse(Encoding.Default.GetString(bytes4, 32, 3), out num2);
                                Encoding.Default.GetString(bytes4, 35, 2);
                                CbmSize = "1";
                                receive_customer_code = Encoding.Default.GetString(bytes4, 37, 10);
                                order_number = Encoding.Default.GetString(bytes4, 47, 32);
                                send_contact = Encoding.Default.GetString(bytes4, 79, 32);
                                send_tel = Encoding.Default.GetString(bytes4, 111, 32);
                                text36 = Encoding.Default.GetString(bytes4, 143, 3);
                                send_address = Encoding.Default.GetString(bytes4, 146, 64);
                                if (text36.Length >= 3)
                                {
                                    num6 = sortedList.IndexOfKey(text36.Substring(0, 3));
                                    if (num6 > -1)
                                    {
                                        tbPost obj4 = (tbPost)sortedList.GetByIndex(num6);
                                        send_city = obj4.city;
                                        send_area = obj4.area;
                                        send_address = send_address.Replace("台", "臺");
                                        send_address = send_address.Replace(send_city, "").Replace(send_area, "");
                                    }
                                }
                                if (send_city == "" && send_area == "" && send_address == "")
                                {
                                    send_city = text5;
                                    send_area = text6;
                                    send_address = text7;
                                }
                                receive_contact = Encoding.Default.GetString(bytes4, 210, 32);
                                receive_tel1 = Encoding.Default.GetString(bytes4, 242, 32);
                                string string5 = Encoding.Default.GetString(bytes4, 274, 3);
                                receive_address = Encoding.Default.GetString(bytes4, 277, 64);
                                text22 = Encoding.Default.GetString(bytes4, 341, 5);
                                if (text22.TrimEnd() != "" && text22.TrimEnd().Length >= 3)
                                {
                                    text10 = text22;
                                }
                                else if (string5.TrimEnd() != "" && string5.TrimEnd().Length >= 3)
                                {
                                    text10 = string5;
                                }
                                if (text10 != "")
                                {
                                    num6 = sortedList.IndexOfKey(text10.Substring(0, 3));
                                    if (num6 > -1)
                                    {
                                        tbPost obj5 = (tbPost)sortedList.GetByIndex(num6);
                                        receive_city = obj5.city;
                                        receive_area = obj5.area;
                                        receive_address = receive_address.Replace("台", "臺");
                                        receive_address = receive_address.Replace(receive_city, "").Replace(receive_area, "");
                                    }
                                }
                                using (SqlCommand sqlCommand4 = new SqlCommand())
                                {
                                    sqlCommand4.Parameters.AddWithValue("@receive_city", receive_city);
                                    sqlCommand4.Parameters.AddWithValue("@receive_area", receive_area);
                                    sqlCommand4.CommandText = "usp_GetDistributor";
                                    sqlCommand4.CommandType = CommandType.StoredProcedure;
                                    using (DataTable dataTable4 = dbAdapter.getDataTable(sqlCommand4))
                                    {
                                        if (dataTable4 != null && dataTable4.Rows.Count > 0)
                                        {
                                            try
                                            {
                                                if (Convert.ToInt32(dataTable4.Rows[0]["status_code"]) == 1)
                                                {
                                                    Distributor = dataTable4.Rows[0]["supplier_code"].ToString();
                                                    area_arrive_code = dataTable4.Rows[0]["area_arrive_code"].ToString();
                                                }
                                            }
                                            catch
                                            {
                                            }
                                        }
                                    }
                                }
                                Encoding.Default.GetString(bytes4, 346, 30);
                                Encoding.Default.GetString(bytes4, 376, 5);
                                if (int.TryParse(Encoding.Default.GetString(bytes4, 381, 8), out result))
                                {
                                    MyIntDateToStr(result);
                                }
                                switch (Encoding.Default.GetString(bytes4, 389, 2))
                                {
                                    case "1":
                                        time_period = "早";
                                        break;
                                    case "2":
                                        time_period = "午";
                                        break;
                                    case "3":
                                        time_period = "晚";
                                        break;
                                    case "4":
                                        time_period = "不指定";
                                        break;
                                }
                                Encoding.Default.GetString(bytes4, 391, 1);
                                int.TryParse(Encoding.Default.GetString(bytes4, 392, 8), out num5);
                                Encoding.Default.GetString(bytes4, 400, 2);
                                Encoding.Default.GetString(bytes4, 402, 6);
                                Encoding.Default.GetString(bytes4, 408, 1);
                                invoice_desc = Encoding.Default.GetString(bytes4, 409, 60);
                                if (Encoding.Default.GetString(bytes4, 469, 1) == "1" && num5 > 0)
                                {
                                    subpoena_category = "41";
                                }
                                Encoding.Default.GetString(bytes4, 470, 10);
                                Encoding.Default.GetString(bytes4, 480, 10);
                                Encoding.Default.GetString(bytes4, 490, 4);
                                Encoding.Default.GetString(bytes4, 494, 1);
                                Encoding.Default.GetString(bytes4, 495, 5);
                                Encoding.Default.GetString(bytes4, 500, 12);
                                Encoding.Default.GetString(bytes4, 512, 21);
                                Encoding.Default.GetString(bytes4, 533, 1);
                                Encoding.Default.GetString(bytes4, 534, 1);
                                Encoding.Default.GetString(bytes4, 535, 7);
                                Encoding.Default.GetString(bytes4, 542, 5);
                                Encoding.Default.GetString(bytes4, 547, 1);
                                Encoding.Default.GetString(bytes4, 548, 1);
                                Encoding.Default.GetString(bytes4, 549, 156);
                                Encoding.Default.GetString(bytes4, 705, 1);
                                Encoding.Default.GetString(bytes4, 706, 1);
                                Encoding.Default.GetString(bytes4, 707, 1);
                                Encoding.Default.GetString(bytes4, 708, 50);
                                Encoding.Default.GetString(bytes4, 758, 5);
                                Encoding.Default.GetString(bytes4, 763, 5);
                                Encoding.Default.GetString(bytes4, 768, 126);
                                num++;
                                string text41 = @"
DeliveryType,
pricing_type,
customer_code,
check_number,
check_type,
subpoena_category,
receive_tel1,
receive_customer_code,
receive_contact,
receive_city,
receive_area,
receive_address,
area_arrive_code,
receive_by_arrive_site_flag,
pieces,plates,
cbm,
collection_money,
arrive_to_pay_freight,
arrive_to_pay_append,
send_contact,send_tel,
send_city,
send_area,
send_address,
product_category,
invoice_desc,
time_period,
arrive_assign_date,
receipt_flag,
pallet_recycling_flag,
cuser,
cdate,
uuser,
udate,
supplier_code,
supplier_name,
sub_check_number,
print_date,supplier_date,
order_number,remote_fee,
print_flag,
turn_board,
upstairs,
difficult_delivery,
turn_board_fee,
upstairs_fee,
difficult_fee, 
Distributor,
CbmSize,
Less_than_truckload,
import_randomCode,
add_transfer,
ProductId,
SpecCodeId";
                                string text42 = "N'" + DeliveryType + "', N'" + pricing_type + "', N'" + customer_code + "', N'" + check_number + "', N'" + check_type + "', N'" + subpoena_category + "', N'" + receive_tel1 + "', N'" + receive_customer_code + "', N'" + receive_contact + "', N'" + receive_city + "', N'" + receive_area + "', N'" + receive_address + "', N'" + area_arrive_code + "', N'" + receive_by_arrive_site_flag + "', N'1', N'0', N'0', N'" + num5 + "', N'0', N'0', N'" + send_contact + "', N'" + send_tel + "', N'" + send_city + "', N'" + send_area + "', N'" + send_address + "', N'" + product_category + "', N'" + invoice_desc + "', N'" + time_period + "', NULL, N'" + receipt_flag + "', N'" + pallet_recycling_flag + "', N'skyeyes', GETDATE(), N'skyeyes', GETDATE(), N'" + supplier_code + "', N'" + supplier_name + "', N'000',N'" + print_date + "',N'" + supplier_date + "',N'" + order_number + "',N'" + remote_fee + "',N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'" + Distributor + "', N'" + CbmSize.ToString() + "', N'1', N'" + fileName + "', N'0'" +", N'" + ProductId + "', N'" + SpecCodeId + "'";
                                //stringBuilder.Append("SET @request_id= 0 ;SET @supplier_fee=0; SET @cscetion_fee=0; INSERT INTO tcDeliveryRequests (" + text41 + ") VALUES(" + text42 + "); Select @request_id=@@Identity ; select @supplier_fee=_fee.supplier_fee   from dbo.fu_GetLTShipFeeByRequestId(@request_id) _fee; Update tcDeliveryRequests set supplier_fee = " + num4 + ",total_fee= " + num4 + " where request_id =@request_id;");
                                vs.Add("SET @request_id= 0 ;SET @supplier_fee=0; SET @cscetion_fee=0; INSERT INTO tcDeliveryRequests (" + text41 + ") VALUES(" + text42 + "); Select @request_id=@@Identity ; select @supplier_fee=_fee.supplier_fee   from dbo.fu_GetLTShipFeeByRequestId(@request_id) _fee; Update tcDeliveryRequests set supplier_fee = " + num4 + ",total_fee= " + num4 + " where request_id =@request_id;");

                            }
                        }
                    }

                    _momolog.insertmomolog(momoget, "Upload開始", downloadPath + fileName + "到" + remoteBackUpPath + fileName);
                    sftpHelper.Upload(downloadPath + fileName, remoteBackUpPath + fileName);
                    _momolog.insertmomolog(momoget, "Upload結束", downloadPath + fileName + "到" + remoteBackUpPath + fileName);
                    _momolog.insertmomolog(momoget, "ChangeDirectory開始", remotePath);
                    sftpHelper.ChangeDirectory(remotePath);
                    _momolog.insertmomolog(momoget, "ChangeDirectory結束", remotePath);
                    _momolog.insertmomolog(momoget, "Delete開始", remotePath + fileName);
                    sftpHelper.Delete(remotePath + fileName);
                    _momolog.insertmomolog(momoget, "Delete結束", remotePath + fileName);
                    _momolog.insertmomolog(momoget, "File刪除開始", ConfigurationManager.AppSettings["DownloadPath"] + fileNameWithoutExtension + ".txt");
                    File.Delete(ConfigurationManager.AppSettings["DownloadPath"] + fileNameWithoutExtension + ".txt");
                    _momolog.insertmomolog(momoget, "File刪除結束", ConfigurationManager.AppSettings["DownloadPath"] + fileNameWithoutExtension + ".txt");

                    List<List<string>> listGroup = new List<List<string>>();
                    int c = 30;//20為一組
                    int jj = c;
                    for (int i = 0; i < vs.Count; i += c)
                    {
                        List<string> cList = new List<string>();
                        cList = vs.Take(jj).Skip(i).ToList();
                        jj += c;
                        listGroup.Add(cList);
                    }
                    int cc = 0;

                    //新增
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, TimeSpan.FromSeconds(600)))
                    {

                        foreach (var list in listGroup)
                        {

                            //  _momolog.insertmomolog(momoget, "SQL insert", (cc + 1).ToString() + " 到 " + (cc + list.Count()).ToString());
                            cc = cc + list.Count();
                            string SQLHeader = "DECLARE @request_id numeric(18, 0);" + "DECLARE @supplier_fee int;" + "DECLARE @cscetion_fee int;";

                            foreach (var l in list)
                            {
                                SQLHeader = SQLHeader + l;
                            }

                            _momolog.insert(SQLHeader);

                            //using (SqlCommand sqlCommand5 = new SqlCommand())
                            //{
                            //    try
                            //    {
                            //        sqlCommand5.CommandText = SQLHeader;
                            //        dbAdapter.execNonQuery(sqlCommand5);
                            //        _momolog.insertmomolog(momoget, "SQL成功", SQLHeader);
                            //    }
                            //    catch (Exception e)
                            //    {
                            //        _momolog.insertmomolog(momoget, "SQL失敗", SQLHeader);
                            //        _Notification_DAL.Insert("GetMomoOrder SQL失敗", e.Message);
                            //        throw e;
                            //    }
                            //}
                        }

                        //才積
                        _CBMDetailLog_DA.Insert(check_numbers, customer_code);

                        scope.Complete();
                    }

                    string fileNameWithoutExtension2 = Path.GetFileNameWithoutExtension(path);
                    fileNameWithoutExtension2 += "_log";

                    string DownloadPathLog = Path.Combine(ConfigurationManager.AppSettings["DownloadPath"], "log", fileNameWithoutExtension2 + ".txt");

                    using (StreamWriter streamWriter = new StreamWriter(DownloadPathLog, append: true))
                    {
                        streamWriter.Write("\r\nLog : ");
                        streamWriter.WriteLine("{0} {1}  檔案：{2}, 執行總筆數：  {3}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString(), Path.GetFileName(path), num.ToString());
                        _momolog.insertmomolog(momoget, "解析TXT結束", string.Format("{0} {1}  檔案：{2}, 執行總筆數：  {3}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString(), Path.GetFileName(path), num.ToString()));
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    num = 0;

                }
            }
            catch (Exception e)
            {
                _momolog.insertmomolog(momoget, "", "Message:" + e.Message + "StackTrace:" + e.StackTrace);
                _Notification_DAL.Insert("GetMomoOrder失敗", e.Message);
            }

            _momolog.insertmomolog(momoget, "End", "OK");
        }



        public static string MyIntDateToStr(int ppDate)
        {
            string text = "";
            if (ppDate > 0 && ppDate < 99991231)
            {
                text = "00000000" + ppDate;
                text = text.Substring(text.Length - 8, 8);
                return text.Substring(0, 4) + "/" + text.Substring(4, 2) + "/" + text.Substring(6, 2);
            }
            return "    /   /  ";
        }

        public static string BytesTohexString(byte[] bytes)
        {
            if (bytes == null || bytes.Count() < 1)
            {
                return string.Empty;
            }
            int num = bytes.Count();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("0x");
            for (int i = 0; i < num; i++)
            {
                string text = Convert.ToString(bytes[i], 16).ToUpper();
                stringBuilder.Append((text.Length == 1) ? ("0" + text) : text);
            }
            return stringBuilder.ToString();
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }

    internal class dbAdapter
    {
        public static SqlConnection getConnection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["JunfuSqlServer"].ToString();
            SqlConnection sqlConnection = new SqlConnection();
            sqlConnection.ConnectionString = connectionString;
            if (sqlConnection.State == ConnectionState.Open)
            {
                sqlConnection.Close();
            }
            sqlConnection.Open();
            return sqlConnection;
        }

        public static DataTable getDataTable(SqlCommand cmd)
        {
            SqlConnection sqlConnection = null;
            DataTable dataTable = null;
            DataTable dataTable2 = null;
            try
            {
                sqlConnection = getConnection();
                dataTable = new DataTable();
                cmd.Connection = sqlConnection;
                new SqlDataAdapter(cmd).Fill(dataTable);
                dataTable2 = new DataTable();
                if (dataTable.Rows.Count <= 0)
                {
                    return dataTable2;
                }
                if (dataTable.Columns.Count <= 0)
                {
                    return dataTable2;
                }
                dataTable2 = dataTable.Clone();
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    dataTable2.ImportRow(dataTable.Rows[i]);
                }
                return dataTable2;
            }
            catch (Exception ex)
            {
                sqlConnection.Close();
                throw ex;
            }
            finally
            {
                sqlConnection.Close();
            }
        }

        public static void execNonQuery(SqlCommand cmd)
        {
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = (cmd.Connection = getConnection());
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                sqlConnection.Close();
                throw ex;
            }
            finally
            {
                sqlConnection.Close();
            }
        }
    }

    internal class downloadFile
    {
        public static void DownloadFile()
        {
            FtpWebRequest obj = (FtpWebRequest)WebRequest.Create("ftp://220.128.210.180:10021/");
            obj.Method = "LIST";
            obj.Credentials = new NetworkCredential("0000077", "0000077");
            FtpWebResponse ftpWebResponse = (FtpWebResponse)obj.GetResponse();
            StreamReader streamReader = new StreamReader(ftpWebResponse.GetResponseStream());
            while (!streamReader.EndOfStream)
            {
                Console.WriteLine(streamReader.ReadLine());
            }
            Console.WriteLine("Directory List Complete, status {0}", ftpWebResponse.StatusDescription);
            streamReader.Close();
            ftpWebResponse.Close();
        }

        public static void DownloadFtpDirectory(string url, NetworkCredential credentials, string localPath)
        {
            FtpWebRequest obj = (FtpWebRequest)WebRequest.Create(url);
            obj.Method = "LIST";
            obj.Credentials = credentials;
            List<string> list = new List<string>();
            using (FtpWebResponse ftpWebResponse = (FtpWebResponse)obj.GetResponse())
            {
                using (Stream stream = ftpWebResponse.GetResponseStream())
                {
                    using (StreamReader streamReader = new StreamReader(stream))
                    {
                        while (!streamReader.EndOfStream)
                        {
                            list.Add(streamReader.ReadLine());
                        }
                    }
                }
            }
            foreach (string item in list)
            {
                string[] array = item.Split(new char[1]
                {
                ' '
                }, 9, StringSplitOptions.RemoveEmptyEntries);
                string text = array[8];
                string obj2 = array[0];
                string text2 = Path.Combine(localPath, text);
                string text3 = url + text;
                if (obj2[0] == 'd')
                {
                    if (!Directory.Exists(text2))
                    {
                        Directory.CreateDirectory(text2);
                    }
                    DownloadFtpDirectory(text3 + "/", credentials, text2);
                }
                else
                {
                    FtpWebRequest obj3 = (FtpWebRequest)WebRequest.Create(text3);
                    obj3.Method = "RETR";
                    obj3.Credentials = credentials;
                    using (FtpWebResponse ftpWebResponse2 = (FtpWebResponse)obj3.GetResponse())
                    {
                        using (Stream stream2 = ftpWebResponse2.GetResponseStream())
                        {
                            using (Stream stream3 = File.Create(text2))
                            {
                                byte[] array2 = new byte[10240];
                                int count;
                                while ((count = stream2.Read(array2, 0, array2.Length)) > 0)
                                {
                                    stream3.Write(array2, 0, count);
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void deleteFtpDirectory(string url, NetworkCredential credentials, string localPath)
        {
            FtpWebRequest obj = (FtpWebRequest)WebRequest.Create(url);
            obj.Method = "LIST";
            obj.Credentials = credentials;
            List<string> list = new List<string>();
            using (FtpWebResponse ftpWebResponse = (FtpWebResponse)obj.GetResponse())
            {
                using (Stream stream = ftpWebResponse.GetResponseStream())
                {
                    using (StreamReader streamReader = new StreamReader(stream))
                    {
                        while (!streamReader.EndOfStream)
                        {
                            list.Add(streamReader.ReadLine());
                        }
                    }
                }
            }
            foreach (string item in list)
            {
                string text = item.Split(new char[1]
                {
                ' '
                }, 9, StringSplitOptions.RemoveEmptyEntries)[8];
                Path.Combine(localPath, text);
                FtpWebRequest obj2 = (FtpWebRequest)WebRequest.Create(url + text);
                obj2.Method = "DELE";
                obj2.Credentials = credentials;
                _ = (FtpWebResponse)obj2.GetResponse();
            }
        }

        public static void deleteFtpFile(string url, NetworkCredential credentials, string fileName)
        {
            FtpWebRequest obj = (FtpWebRequest)WebRequest.Create(url + fileName);
            obj.Method = "DELE";
            obj.Credentials = credentials;
            _ = (FtpWebResponse)obj.GetResponse();
        }
    }

    internal class uploadFile
    {
        public static void UploadFtpDirectory(string uploadurl, NetworkCredential credentials, string localPath)
        {
            string[] files = Directory.GetFiles(localPath, "*.*");
            foreach (string text in files)
            {
                FtpWebRequest obj = (FtpWebRequest)WebRequest.Create(uploadurl + Path.GetFileName(text));
                obj.Credentials = credentials;
                obj.Method = "STOR";
                byte[] array = new byte[new FileInfo(text).OpenRead().Length];
                Stream requestStream = obj.GetRequestStream();
                requestStream.Write(array, 0, array.Length);
                requestStream.Close();
            }
        }

        public static void UploadFtpFile(string uploadurl, NetworkCredential credentials, string filename)
        {
            FtpWebRequest obj = (FtpWebRequest)WebRequest.Create(uploadurl + Path.GetFileName(filename));
            obj.Method = "STOR";
            obj.Credentials = credentials;
            byte[] array = File.ReadAllBytes(filename);
            obj.ContentLength = array.Length;
            Stream requestStream = obj.GetRequestStream();
            requestStream.Write(array, 0, array.Length);
            requestStream.Close();
            FtpWebResponse ftpWebResponse = (FtpWebResponse)obj.GetResponse();
            Console.WriteLine("Upload File Complete, status {0}", ftpWebResponse.StatusDescription);
            ftpWebResponse.Close();
        }
    }
}

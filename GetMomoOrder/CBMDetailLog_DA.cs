﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetMomoOrder
{
    public class CBMDetailLog_DA
    {
        private readonly string connectionString = ConfigurationManager.ConnectionStrings["JunfuSqlServer"].ToString();

        public void Insert(List<string> CheckNumbers, string CreateUser)
        {

           List< CBMDetailLog_Condition> cBMDetailLog_s = new List<CBMDetailLog_Condition>();

            foreach (var CheckNumber in CheckNumbers)
            {
                CBMDetailLog_Condition data = new CBMDetailLog_Condition();
                data.ComeFrom = "1";
                data.CBM = "S060";
                data.CheckNumber = CheckNumber;
                data.CreateUser = CreateUser;
                cBMDetailLog_s.Add(data);
            }
         

            


            using (var cn = new SqlConnection(connectionString))
            {
                string sql =
                     @"  
INSERT INTO [dbo].[CBMDetailLog]
           (
		   [ComeFrom]
           ,[CheckNumber]
           ,[CBM]
           ,[Length]
           ,[Width]
           ,[Height]
          
           ,[CreateUser]
         
		   )
     VALUES
           (
		    @ComeFrom
           ,@CheckNumber
           ,@CBM
           ,@Length
           ,@Width
           ,@Height
           ,@CreateUser
         
		   )
                    ";
                cn.Execute(sql, cBMDetailLog_s);
            }
        }
    }

    public class CBMDetailLog_Condition
    {
        public long id { get; set; }
        public string ComeFrom { get; set; }
        public string CheckNumber { get; set; }
        public string CBM { get; set; }
        public int? Length { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateUser { get; set; }



    }
}

﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetMomoOrder
{
    public class Notification_DAL
    {

        private readonly string connectionString = ConfigurationManager.ConnectionStrings["JunfuSqlServer"].ToString();

        public void Insert(string Subject, string Body)
        {
            Notification_Condition data = new Notification_Condition();
            data.Type = 1;
            data.Body = Body;
            data.Subject = Subject;
            data.CreateUser = "GetMomoOrder";

            data.Recipient = ConfigurationManager.AppSettings["Authorization"];


            using (var cn = new SqlConnection(connectionString))
            {
                string sql =
                   @"  
                    INSERT [dbo].[Notification] 
                    (
       [Type]
      ,[Recipient]
      ,[Subject]
      ,[Body]
      ,[CreateUser]

                    ) VALUES
                    (   
       @Type
      ,@Recipient
      ,@Subject
      ,@Body
      ,@CreateUser

                    )
                    ";
                cn.Execute(sql, data);
            }
        }
    }

    public class Notification_Condition
    {
        public long id { get; set; }
        public int Type { get; set; }
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreateUser { get; set; }
    }
}
